/**
 * Created by longqi on 18/Mar/17.
 */
chrome.runtime.onMessage.addListener(
    function (message, sender, sendResponse) {
        if (message.name == 'zhang longqi' && !sender.tab) {
            console.debug('listener: ', message.selector);
            rotate(message.selector);
        }
        sendResponse({farewell: "goodbye"});
        return true;
    });

function rotate(selector) {
    let docs = document.querySelectorAll(selector);
    for (let doc of docs) {
        let current_transform = doc.style.transform;
        let index = current_transform.indexOf('rotate(');
        let angle = index == -1 ? 0 : parseInt(current_transform.slice(index + 7, -4)) % 360;

        angle += 90;

        doc.style.transform = 'rotate(' + angle + 'deg)';
        console.debug('Rotate your elements ', selector, ' ', angle, ' deg')
    }
}
