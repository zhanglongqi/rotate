function send_message(selector = 'video') {
    chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id, {
            name: 'zhang longqi',
            selector: selector
        }, function (response) {
            console.info(response);
        });
    });
}
document.addEventListener('DOMContentLoaded', function () {

    let resetButton = document.getElementById('reset');
    let rotateButton = document.getElementById('rotate');
    resetButton.addEventListener('click', function () {
        console.log('reset all the rotation')
    });
    rotateButton.addEventListener('click', function () {
        let selector = document.querySelector('#selector').value;
        console.debug('click event', selector);
        send_message(selector == '' ? 'video' : selector);
    });

});

function getSettings(next) {
    chrome.storage.local.get(['setting_huaci', 'setting_auto_pronounce', 'setting_auto_add_to_my_note'], function (items) {
        next(items);
    });
}

function setSettings(obj, next) {
    chrome.storage.local.set(obj, next);
}